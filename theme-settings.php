<?php

/**
 * Implementation of hook_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function particle_settings($saved_settings) {
  $defaults = array(
      'particle_show_date' => 1
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['particle_show_date'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide date widget on Pages'),
      '#default_value' => $settings['particle_show_date'],
  );

  // Return the additional form widgets
  return $form;
}