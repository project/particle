<?php // $Id$  ?>
<!doctype html>
<html lang="<?php print $language->language; ?>">
  <head>
    <?php print $head; ?>
    <meta charset=utf-8>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="container">
      <div id="header">

        <?php if (!empty($site_name)): ?>
        <h1 id="site-name">
          <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
        </h1>
        <?php endif; ?>

        <?php if ($primary_links): ?>
        <div id="navigation" class="clear-block">
        <?php print $navigation; ?>
        </div>
        <?php endif; ?>

      </div>
    <div id="content">
      <div id="main">
        <?php print $tabs; ?>
        <?php print $content; ?>
      </div>

      <?php if (!empty($sidebar)): ?>
      <div id="sidebar">
      <?php print $sidebar; ?>
      </div>
      <?php endif; ?>

    </div>

    <div id="footer">
    <?php print $footer; ?>
      <p id="back-top"><a href="#header">Back to top</a></p>
    </div>
    </div>
  </body>
</html>